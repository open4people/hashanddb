﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Threading;

namespace ConsoleApplication1
{
    class Saver: IObservable
    {
        static Semaphore semaphor;
        private List<IObserver> observers;
        private int savedFileCount = 0;
        Thread thread;
        
        DbAdapter dbadapter;
        

        private Saver() { }
        public static Saver Instance
        {
            get
            {
                return Singleton<Saver>.Instance;
            }
        }


        public void Init()
        {
            dbadapter = new DbAdapter();            
            observers = new List<IObserver>();            
            semaphor = new Semaphore(1, 5);
            thread = new Thread(new ParameterizedThreadStart(Run));
        }

        public void AttachObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        public void NotifyObservers(Object changedObj)
        {
            foreach (IObserver observer in observers)
                observer.Update(changedObj);
        }
        
        public void Save(string hash)
        {            
            semaphor.WaitOne();
            dbadapter.Insert(hash);
            savedFileCount++;
            semaphor.Release();
        }

        public void StartThread(object state)
        {            
            thread.Start(state);
        }


        public void Run(object state)
        {
            do
            {
                string infostr = String.Format("saved {0} file(s) / founded {1} file(s)", savedFileCount, Reader.Instance.FileCount);
                NotifyObservers(infostr);
                Thread.Sleep(100);
            } while ( ((Conveer)state).state != ConveerState.STOP);
        }

    }
}
