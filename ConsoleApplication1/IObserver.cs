﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    interface IObserver
    {
        void Update(Object obj);
    }

    interface IObservable
    {
        void AttachObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers(Object changedObj);
    }
}
