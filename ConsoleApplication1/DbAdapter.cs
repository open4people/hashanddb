﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Web;

using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.Linq;
using System.Configuration;


namespace ConsoleApplication1
{
    class DbAdapter
    {
        public DbAdapter() {}

        public void Insert(string hash)
        {
            SqlConnection connection;
            string connectionString;
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            connection = new SqlConnection(connectionString);

            string sqlexpression = String.Format("INSERT INTO [dbo].[Hashes] (Hash) VALUES ('{0}');", hash);
            using (connection)
            {
                try
                {
                    connection.Open();
                    SqlCommand sqlcommand = new SqlCommand(sqlexpression, connection);
                    int number = sqlcommand.ExecuteNonQuery();                    
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }                
            }
        }
    }
}
