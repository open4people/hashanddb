﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Threading;

namespace ConsoleApplication1
{
    enum ReaderState
    {
        FIRSTWAIT,
        WAIT,
        READ,
        STOP
    };
    class Reader: IObservable
    {
        private List<IObserver> observers;
        Semaphore semaphor;
        Thread thread;
        public int FileCount { get; set; }
        public ReaderState state { get; set; }

        private Reader() { state = ReaderState.FIRSTWAIT;}

        public static Reader Instance
        {
            get
            {
                return Singleton<Reader>.Instance;
            }
        }

        public void Init() 
        {
            semaphor = new Semaphore(1, 2);
            observers = new List<IObserver>();
            thread = new Thread(new ParameterizedThreadStart(ReadFiles));
        }

        public void AttachObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        public void NotifyObservers(Object changedObj)
        {
            foreach (IObserver observer in observers)
                observer.Update(changedObj);
        }
        
        public void ReadFiles(object Paths)
        {            
            foreach (string path in (string[])Paths)
            {
                string[] _tmpfiles = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
                foreach (string file in _tmpfiles)
                {
                    semaphor.WaitOne();
                    state = ReaderState.READ;
                    FileCount++;
                    //NotifyObservers("readed:" + file);
                    Conveer.Instance.AddEntity(file);
                    state = ReaderState.WAIT;
                    semaphor.Release();
                }
            }
            state = ReaderState.STOP;
        }

        public void StartThread(string[] directories)
        {            
            thread.Start(directories);
        }

    }
}
