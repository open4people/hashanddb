﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Security.Cryptography;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            MainTask(); //read,conv, save ...
            //Dbtask();
            Console.ReadLine();
        }
        static void Dbtask()
        {
            Saver.Instance.Init();
            Saver.Instance.Save("some hash");            
        }
        static void MainTask()
        {
            Facade facade = new Facade();
            facade.PrepareObserver();
            facade.StartThreads();
        }
    }
}
