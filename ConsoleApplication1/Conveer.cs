﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

namespace ConsoleApplication1
{
    enum ConveerState
    {
        FIRSTWAIT,
        WAIT,
        READIN, //read input
        WRITEOUT,//
        STOP
    }
    class Conveer: IObservable
    {
        private List<IObserver> observers;
        static Queue<FileEntity> filesname;
        static Semaphore semaphor;
        Thread[] threads;
        public ConveerState state { get; set; }
        private Conveer() { state = ConveerState.FIRSTWAIT; }

        public static Conveer Instance
        {
            get
            {
                return Singleton<Conveer>.Instance;
            }
        }

        public void Init(int threadcount=2) 
        {            
            observers = new List<IObserver>();
            filesname = new Queue<FileEntity>();
            semaphor = new Semaphore(threadcount, threadcount);
            threads = new Thread[threadcount];
            for (int i = 0; i < threadcount; i++)
            {
                threads[i] = new Thread(new ParameterizedThreadStart(Run));
            }
        }

        public void AttachObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        public void NotifyObservers(Object changedObj)
        {
            foreach (IObserver observer in observers)
                observer.Update(changedObj);
        }
        

        public void AddEntity(string file)
        {
            semaphor.WaitOne();
            state = ConveerState.READIN;
            FileEntity f = new FileEntity(file);
            filesname.Enqueue(f);//somethimes catch bug
            //NotifyObservers("files :" + filesname.Count);
            state = ConveerState.WAIT;
            semaphor.Release();
        }

        public void StartThreads(object reader)
        {
            foreach(Thread t in threads)
            {
                t.Start(reader);
            }
        }

        private void Run(object reader)
        {
            do
            {
                if (filesname.Count > 0)
                {
                    semaphor.WaitOne();
                    this.state = ConveerState.WRITEOUT;
                    FileEntity filename = filesname.Dequeue();
                    //Thread.Sleep(1000);
                    Saver.Instance.Save(filename.GetHash());
                    this.state = ConveerState.WAIT;
                    semaphor.Release();
                }
            } while (((Reader)reader).state != ReaderState.STOP);
            this.state = ConveerState.STOP;
        }
    }
}
