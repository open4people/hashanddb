﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Threading;

using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace ConsoleApplication1
{
    class Facade:IObserver
    {
        Saver saver;
        Reader reader;
        Conveer conv;
        
        public static bool islogging = true;
        int maxvieweablerow = 1;
        int vieweablerow = 0;
        string[] dirsName = { @"C:\Python27\DLLs", @"F:\" };
        
        

        public Facade()
        {
            //files = new Queue<FileEntity>();            
            reader = Reader.Instance;
            conv = Conveer.Instance;
            saver = Saver.Instance;
            

            reader.Init();
            conv.Init(5);
            saver.Init();
        }

        

        public void Update(Object changedObj)
        {
            if ((islogging) && (changedObj != null))
            {
                Console.WriteLine(String.Format("Upd:{0}", (string)changedObj));
                vieweablerow++;
                Thread.Sleep(50);
            }
            if (vieweablerow == maxvieweablerow)
            {
                Console.Clear();
                vieweablerow = 0;
            }
        }

        public void PrepareObserver()
        {
            reader.AttachObserver(this);
            conv.AttachObserver(this);
            saver.AttachObserver(this);
        }

        public void StartThreads()
        {
            reader.StartThread(dirsName);            
            conv.StartThreads(reader);
            saver.StartThread(conv);
        }

    }
}
