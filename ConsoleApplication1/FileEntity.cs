﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;

using System.Data.Linq.Mapping;

namespace ConsoleApplication1
{
    public class FileEntity : IHashable
    {
        public FileEntity(string name) { this.Name = name; }

        public string Name { get; set; }
        public string GetHash()
        {
            MD5 md5hasher = MD5.Create();
            byte[] data = md5hasher.ComputeHash(Encoding.Default.GetBytes(Name));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public override string ToString()
        {
            return Name + ":" + GetHash();
        }
    }
}
